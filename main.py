#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication, QWidget

import myWidget 
from myWidget import *


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyQtWidget()
    sys.exit(app.exec_())
