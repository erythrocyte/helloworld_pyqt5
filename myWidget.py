#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os, inspect, platform
# from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import (QWidget, QToolTip, QPushButton, QApplication,
        QDesktopWidget, QMainWindow, QFileDialog)
from PyQt5.QtGui import QFont

# class MyQtWidget(QWidget):
class MyQtWidget(QMainWindow):
    def __init__(self):
        super(MyQtWidget, self).__init__()

        self.initUI()


    def initUI(self):

        QToolTip.setFont(QFont('SansSerif', 10))

        self.setToolTip('This is a <b>QWidget</b> widget')

        btn = QPushButton('Button', self)
        btn.setToolTip('This is a <b>QPushButton</b> widget')
        btn.resize(btn.sizeHint())
        btn.move(50, 50)
        btn.clicked.connect(self.buttonRunProgClicked)

        self.statusBar()

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Tooltips')
        self.center()
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def buttonRunProgClicked(self):

        sender = self.sender()
        self.statusBar().showMessage(sender.text() + ' was pressed')
        
        # print inspect.getfile(inspect.currentframe()) # script filename (usually with path)
        # cf = inspect.getfile(inspect.currentframe())
        # ss = os.path.dirname(os.path.abspath(cf)) # script directory
        # comm = "cd " + ss + "/external/"
        # print comm
        _file = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        # print _file
        # os.system ("cd " + ss + "/external")
        # os.system("ls")
        # os.system(_file +  "/./a " + _file)
        _platform = platform.system()
        if (_platform == "Linux"):
            os.system("external/lin/./a " + _file)
        else:
            os.system("external/win/a.exe " + _file)

        # print os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) # script directory
        # os.system("cd /external")
        # os.system("./a")
